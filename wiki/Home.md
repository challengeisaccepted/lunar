#Experience Simplicity

Welcome to the Lunar wiki! In case you haven't already read the description for Lunar, my intent for the project is thus:

* Extensible API which allows for SIMPLE addons

* Drag-n'-drop interface, or optionally a command line if you find that typing is faster

* Keyboard shortcuts

* Package management (compress and decompress programs into their original state)

* LuaDocs - intended to be a commenting system which will produce a visual appeasing and easy to search "wiki" document

* Jump-to-line: If you have an error, Lunar will give you the option to jump to that line for easy correction.

* Project and file searching


If you take a look around, I'm sure you'll notice that this project looks empty - and you'd be right! I've gone through about ten different versions of the project since its inception a few months back and haven't been happy with any of them. I'm working diligently to get it done, but progress is slow. If you have any interest in helping me out, feel free to contact me on the ComputerCraft forums under the username Bubba.