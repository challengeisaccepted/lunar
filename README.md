Welcome to Lunar! This project is aimed towards Lua programmers who find it annoying and/or difficult to use the default editing environment. It is still heavily in development, but my future
vision for the project is thus:

* Extensible API which allows for SIMPLE addons

* Drag-n'-drop interface, or optionally a command line if you 
find that typing is faster

* Keyboard shortcuts

* Package management (compress and decompress programs into their original state)

* LuaDocs - intended to be a commenting system which will produce a visual appeasing and easy to search "wiki" document

* Jump-to-line: If you have an error, Lunar will give you the option to jump to that line for easy correction.

* Project and file searching


----
Obviously Lunar is nowhere near completion - in fact, right now it is little more than a concept with a fairly dedicated programmer behind the scenes working on it (AKA me). I've gone through probably ten different executions of this project and disliked each one, mainly because I felt that it was not extensible enough or because it limited the user in same way.